<?php

declare(strict_types=1);

namespace Fuppes\ApiFootballComClient\Metrics;

use Artprima\PrometheusMetricsBundle\Metrics\MetricsCollectorInterface as ParentInterface;

interface MetricsCollectorInterface extends ParentInterface
{
    public function incRequests(string $label): void;

    public function refreshRequestsRemaining(int $value): void;
}
