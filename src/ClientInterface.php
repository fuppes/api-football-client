<?php

namespace Fuppes\ApiFootballComClient;

use Fuppes\ApiFootballComClient\Endpoint\League;
use Fuppes\ApiFootballComClient\Endpoint\Fixture;
use Fuppes\ApiFootballComClient\Endpoint\Player;
use Fuppes\ApiFootballComClient\Endpoint\Standings;
use Fuppes\ApiFootballComClient\Endpoint\Status;
use Fuppes\ApiFootballComClient\Endpoint\Team;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Fuppes\ApiFootballComClient\Metrics\MetricsCollectorInterface;
use Psr\Log\LoggerInterface;

interface ClientInterface
{
    public function request(string $method, string $url, array $options = []): ResponseInterface;

    public function getMetricsCollector(): MetricsCollectorInterface;

    public function getLogger(): LoggerInterface;

    public function fixture(): Fixture;

    public function league(): League;

    public function team(): Team;

    public function standings(): Standings;

    public function player(): Player;

    public function status(): Status;
}
