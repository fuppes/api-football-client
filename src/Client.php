<?php

namespace Fuppes\ApiFootballComClient;

use Psr\Log\LoggerInterface;
use Fuppes\ApiFootballComClient\Endpoint\Team;
use Fuppes\ApiFootballComClient\Endpoint\League;
use Fuppes\ApiFootballComClient\Endpoint\Player;
use Fuppes\ApiFootballComClient\Endpoint\Status;
use Fuppes\ApiFootballComClient\Endpoint\Fixture;
use Fuppes\ApiFootballComClient\Endpoint\Standings;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Fuppes\ApiFootballComClient\Metrics\MetricsCollectorInterface;

final class Client implements ClientInterface
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly ?MetricsCollectorInterface $metricsCollector = null,
        private ?array $endpoints = [],
    ) {

    }

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $response = $this->httpClient->request($method, $url, $options);

        $responseHeaders = $response->getHeaders();
        $requestsRemainingArray = $responseHeaders['x-ratelimit-requests-remaining'] ?? [];
        $requestsRemaining = $requestsRemainingArray[0] ?? null;

        if ($requestsRemaining) {
            $this->metricsCollector->refreshRequestsRemaining((int)$requestsRemaining);
        }

        return $response;
    }

    public function setMetricsCollector(MetricsCollectorInterface $metricsCollector): self
    {
        $this->metricsCollector = $metricsCollector;

        return $this;
    }

    public function getMetricsCollector(): MetricsCollectorInterface
    {
        return $this->metricsCollector;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function league(): League
    {
        return $this->endpoints['league'] ?? ($this->endpoints['league'] = new League($this));
    }

    public function fixture(): Fixture
    {
        return $this->endpoints['fixture'] ?? ($this->endpoints['fixture'] = new Fixture($this));
    }

    public function team(): Team
    {
        return $this->endpoints['team'] ?? ($this->endpoints['team'] = new Team($this));
    }

    public function standings(): Standings
    {
        return $this->endpoints['standings'] ?? ($this->endpoints['standings'] = new Standings($this));
    }

    public function player(): Player
    {
        return $this->endpoints['player'] ?? ($this->endpoints['player'] = new Player($this));
    }

    public function status(): Status
    {
        return $this->endpoints['status'] ?? ($this->endpoints['status'] = new Status($this));
    }
}
