<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Symfony\Component\HttpFoundation\Response;

final class League extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'league';

    protected const PATH = 'leagues';

    public function list(): Response
    {
        $method = "GET";
        $response = $this->client->request($method, $this->compileUrl(), $this->getOptions());
        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function league(
        ?int $id = null,
        ?string $name = null,
        ?string $country = null,
        ?string $code = null,
        ?int $season = null,
        ?int $team = null,
        ?string $type = null,
        ?string $current = null,
        ?string $search = null,
        ?int $last = null,
    ): Response {
        $method = "GET";

        $params = [
            'id' => $id,
            'name' => $name,
            'country' => $country,
            'code' => $code,
            'season' => $season,
            'team' => $team,
            'type' => $type,
            'current' => $current,
            'search' => $search,
            'last' => $last,
        ];

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
