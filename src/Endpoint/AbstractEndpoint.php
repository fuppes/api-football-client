<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Psr\Log\LoggerInterface;
use Fuppes\ApiFootballComClient\ClientInterface;

abstract class AbstractEndpoint
{
    protected const PATH = '';

    public function __construct(
        protected readonly ClientInterface $client,
    ) {
    }

    protected function getOptions(): array
    {
        // call api-football.com if not found in elasticsearch
        $headers = [
           'headers' => [
               'x-rapidapi-host' => $_ENV['API_FOOTBALL_BASE_URL'],
               'x-rapidapi-key' => $_ENV['API_FOOTBALL_API_KEY'],
           ],
        ];

        $options = array_merge(
            $headers,
        );

        return $options;
    }

    protected function compileUrl(array $params = [], string $urlSuffix = null): string
    {
        $url = sprintf(
            "https://v3.football.api-sports.io/%s",
            static::PATH . $urlSuffix ?? ""
        );

        $paramsCollection = [];

        foreach ($params as $key => $value) {
            if (null !== $value) {
                $paramsCollection[] = sprintf(
                    "%s=%s",
                    $key,
                    $value
                );
            }
        }

        if ($paramsCollection) {
            $url = $url . '?'
            . implode('&', $paramsCollection);
        }

        return $url;
    }
}
