<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Symfony\Component\HttpFoundation\Response;

final class Player extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'player';

    protected const PATH = 'players';

    public function player(int $id, int $season): Response
    {
        $method = "GET";

        $params = [
            'id' => $id,
            'season' => $season
        ];

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function playersByTeam(int $team, int $season): Response
    {
        $method = "GET";

        $params = [
            'team' => $team,
            'season' => $season
        ];

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
