<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Symfony\Component\HttpFoundation\Response;

final class Team extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'team';

    protected const PATH = 'teams';

    public function information(
        int $id
    ): Response {

        $method = "GET";

        $params = [
            'id' => $id
        ];

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());
        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function seasons(int $team): Response
    {

        $method = "GET";

        $params = [
            'team' => $team
        ];

        $response = $this->client->request($method, $this->compileUrl($params, "/seasons"), $this->getOptions());
        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
