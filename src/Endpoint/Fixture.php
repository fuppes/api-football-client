<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use DateTime;
use Symfony\Component\HttpFoundation\Response;

final class Fixture extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'fixture';

    protected const PATH = 'fixtures';

    public function fixtures(
        ?int $id = null,
        ?array $ids = null,
        ?string $live = null,
        ?string $date = null,
        ?int $league = null,
        ?int $leagueId = null,
        ?int $season = null,
        ?int $team = null,
        ?int $last = null,
        ?int $next = null,
        ?string $from = null,
        ?string $to = null,
        ?string $round = null,
        ?string $status = null,
        ?int $venue = null,
        ?string $timezone = null,
    ): Response {
        $method = "GET";
        $params = [
            'id' => $id,
            'live' => $live,
            'date' => $date,
            'league' => $league ?? $leagueId ?? null,
            'season' => $season,
            'team' => $team,
            'last' => $last,
            'next' => $next,
            'round' => $round,
            'status' => $status,
            'venue' => $venue,
            'timezone' => $timezone,
        ];

        if ($ids) {
            $params['ids'] = implode("-", $ids);
        }

        if ($from) {
            $params['from'] = (new DateTime($from))->format('Y-m-d');
        }

        if ($to) {
            $params['to'] = (new DateTime($to))->format('Y-m-d');
        }

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function players(int $fixtureId): Response
    {
        $method = "GET";
        $params = [
            'fixture' => $fixtureId,
        ];

        $response = $this->client->request($method, $this->compileUrl($params, "/players"), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function statistics(int $fixtureId): Response
    {
        $method = "GET";
        $params = [
            'fixture' => $fixtureId,
        ];

        $response = $this->client->request($method, $this->compileUrl($params, "/statistics"), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function lineups(int $fixtureId): Response
    {
        $method = "GET";
        $params = [
            'fixture' => $fixtureId,
        ];

        $response = $this->client->request($method, $this->compileUrl($params, "/lineups"), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }

    public function headToHead(int $homeTeamId, int $awayTeamId): Response
    {
        $method = "GET";
        $params = [
            'h2h' => sprintf('%d-%d', $homeTeamId, $awayTeamId),
        ];

        $response = $this->client->request($method, $this->compileUrl($params, "/headtohead"), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
