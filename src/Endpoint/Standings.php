<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Symfony\Component\HttpFoundation\Response;

final class Standings extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'standings';

    protected const PATH = 'standings';

    public function standings(
        ?int $league = null,
        int $season,
        ?int $team = null,
    ): Response {
        $method = "GET";

        $params = [
            'league' => $league,
            'season' => $season,
            'team' => $team,
        ];

        $response = $this->client->request($method, $this->compileUrl($params), $this->getOptions());

        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
