<?php

namespace Fuppes\ApiFootballComClient\Endpoint;

use Symfony\Component\HttpFoundation\Response;

final class Status extends AbstractEndpoint
{
    private const METRICS_LABEL_LOWERCASE = 'status';

    protected const PATH = 'status';

    public function status(): Response
    {
        $method = "GET";

        $response = $this->client->request($method, $this->compileUrl(), $this->getOptions());
        $this->client->getMetricsCollector()->incRequests(self::METRICS_LABEL_LOWERCASE);

        return new Response($response->getContent());
    }
}
